/*  Table structure 
    This script is DB design. It is meant to bootstrap the tables and columns
    of a database, to be replaced with a dump for regular backups. 
    If I have anything wrong, please let me know. This is coding outloud here.
*/

DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id integer(11) NOT NULL AUTO_INCREMENT,
    email varchar(50) DEFAULT NULL,
//* joined date */
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS contact_info;
CREATE TABLE contact_info (
    id integer(11) NOT NULL AUTO_INCREMENT,
    user_id integer(11) NOT NULL,
    phone varchar(50) DEFAULT NULL,
    address integer(11) DEFAULT NULL,
    registered boolean DEFAULT FALSE,
    PRIMARY KEY (id)
);

/* TODO: WE NEED TABLES FOR THE jQuery CMS SYSTEM!!! */
