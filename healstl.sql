/* TODO: In my UML chart, change every DATETIME to TIMESTAMP. */
/* TODO: In the UML chart, I forgot to add user information for the admins. */

CREATE DATABASE IF NOT EXISTS healstl;
USE DATABASE healstl;

/* Phase 1: Essential CMS */
CREATE TABLE IF NOT EXIST users (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 email VARCHAR(100) NOT NULL,
 pword PASSWORD NOT NULL,
 PRIMARY KEY(id)
);

/* We have two types of users: Adminstrators and volunteers */
CREATE TABLE IF NOT EXISTS admins (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 email VARCHAR(100) NOT NULL,
 join_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 phone_num VARCHAR(10) NOT NULL,
 title_name VARCHAR(5),
 first_name VARCHAR(25) NOT NULL,
 middle_name VARCHAR(20),
 last_name VARCHAR(25) NOT NULL,
 suffix_name VARCHAR(5),
 dob DATE NOT NULL,
 address1 VARCHAR(40),
 address2 VARCHAR(20),
 city VARCHAR(30),
 state VARCHAR(2),
 zip_code VARCHAR(5),
 job_title VARCHAR(20),
 can_adminstrate ENUM('no','yes') DEFAULT 'no',                    /* is the admin an adminstrator? */
 can_moderate ENUM('no','yes') DEFAULT 'no',                       /* is the admin a moderator? */
 can_contribute ENUM('no','yes') DEFAULT 'no',                     /* can this admin contribute content to the site? */
 can_contact ENUM('no','yes') DEFAULT 'no',                        /* can you contact this admin */
 terminated ENUM('no','quit','supended','fired') DEFAULT 'no',
 term_when DATE,
 term_reason VARCHAR(255),
 PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS volunteers (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 join_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 email VARCHAR(100) NOT NULL,
 phone_num VARCHAR(10) NOT NULL,
 title_name VARCHAR(5),
 first_name VARCHAR(25) NOT NULL,
 middle_name VARCHAR(20),
 last_name VARCHAR(25) NOT NULL,
 suffix_name VARCHAR(5),
 dob DATE NOT NULL,
 address1 VARCHAR(40),
 address2 VARCHAR(20),
 city VARCHAR(30),
 state VARCHAR(2),
 zip_code VARCHAR(5),
 volunteering ENUM('no','yes') DEFAULT 'yes',
 notifications ENUM('no','near','all') DEFAULT 'all',
 note_dist INTEGER,
 subscribed ENUM('no','yes') DEFAULT 'yes',
 registered ENUM('no','yes') DEFAULT 'no',
 can_contribute ENUM('no','yes') DEFAULT 'no',             /* Only an admin can set this setting. Basically, can the user contribute to ideas for volunteer events. */
 PRIMARY KEY(id)
);

/* Admins control most of the content of the site */
/* How do I find latitude and logitude of both events and volunteers to calculate distance (as the crow flies)? Should I put locations in their own table? */
CREATE TABLE IF NOT EXISTS events (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(255) NOT NULL,
 address1 VARCHAR(40),
 address2 VARCHAR(20),
 city VARCHAR(30),
 state VARCHAR(2),
 zip_code VARCHAR(5),
 start_when DATETIME,
 end_when DATETIME,
 description TEXT NOT NULL,
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS newsletters (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 broadcast_when DATETIME NOT NULL,
 contents TEXT NOT NULL,
 description TEXT NOT NULL,
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* Volunteers control what events they RSVP */
CREATE TABLE IF NOT EXISTS event_vols (
 evt_id INTEGER,
 vol_id INTEGER,
 vol_status ENUM('no','maybe','yes') DEFAULT 'no',
 vol_rsvp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 FOREIGN KEY(evt_id) REFERENCES events(id),
 FOREIGN KEY(vol_id) REFERENCES volunteers(id),
);

/* Phase 2: File CMS */
CREATE TABLE IF NOT EXISTS folders (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(30) NOT NULL,
 description VARCHAR(255),
 parent_id INTEGER(11),                   /* Refers to the ID of another folders record if needed. This is nullable because the root (top) folder has no parent_id. */
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 access ENUM('private','admins','public') DEFAULT 'private',
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);


/* For more infor on Uploading files to a MySQL database using PHP visit
 * http://www.php-mysql-tutorial.com/wikis/mysql-tutorials/uploading-files-to-mysql-database.aspx
 */
CREATE TABLE IF NOT EXISTS files (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(30) NOT NULL,
 type VARCHAR(30) NOT NULL,
 size INTEGER NOT NULL,
 content MEDIUMBLOB NOT NULL,           /* MEDIUMBLOBs can hold up to 16MB of binary data */
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 access ENUM('private','admins','public') DEFAULT 'private',
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* galleries are like folders but for photos only */
CREATE TABLE IF NOT EXISTS galleries (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(30) NOT NULL,
 parent_id INTEGER(11),
 description VARCHAR(255),
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 access ENUM('private','admins','public') DEFAULT 'private',
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* TODO: Find out some way to store thumbnails of photos in this table.  Better yet, create another table for storing thumbnails. */
CREATE TABLE IF NOT EXISTS photos (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(30) NOT NULL,
 type VARCHAR(30) NOT NULL,
 size INTEGER NOT NULL,             /* in bytes */
 width INTEGER NOT NULL,            /* in pixels */
 height INTEGER NOT NULL,           /* in pixels */
 content MEDIUMBLOB NOT NULL,
 description VARCHAR(255),          /* a short description of this photo */
 attribution VARCHAR(255),          /* attribution, or credit, is manditory if you are using someone elses photos. Generally, I like to put it in the alt and title attributes of an <img> element so that when the user mouseovers the element, they can see the credit.  More sophisticted code would paint a small watermark in the coroner (which ImageMagick can do) to show attribution. */
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 access ENUM('private','admins','public') DEFAULT 'private',
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* A thumbnail is a smaller version of a photo.  Thumbnails can be useful for galleries or for a reduced size photo in articles. */
/* NOTE: The publishing status of a thumbnail depends on the status of the photo it represents. */
/* Thumbs should be automatically generated using PHP and ImageMagick (assuming that is installed with PHP).
 * If the user requests a thumb of a particular size or scale, two thumbs will be created:
 * One that is the standard size of presenting the photo in a gallery and the other resized at the user's request.
 * Better yet, let's create another table that stores user-requested scaled thumbs in its own table
 * and use this table strictly for the thumb that is used in the gallery.
 * Thumb creation doesn't need to be regulated. If a photo is deleted, so should its thumbs
 */
CREATE TABLE IF NOT EXISTS thumbs (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 photo_id INTEGER(11) NOT NULL,     /* Should be the ID of the photo the thumb represents. */
 name VARCHAR(30) NOT NULL,
 type VARCHAR(30) NOT NULL,
 size INTEGER NOT NULL,             /* in bytes */
 width INTEGER NOT NULL,            /* in pixels */
 height INTEGER NOT NULL,           /* in pixels */
 scale FLOAT NOT NULL,              /* in percent */
 content MEDIUMBLOB NOT NULL,
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 PRIMARY KEY(id),
 FOREIGN KEY(photo_id) REFERENCES photos(id),
 FOREIGN KEY(created_by) REFERENCES admins(id)
);

/* Let's call the user-defined scaled thumbs figures.  Since these will be inserted into articles and other content,
 * and their size isn't the same as a standard thumbnail, a reduced-size non-thumbnail picture can be stored here
 * in various sizes at the user's preference.
 * Fun fact: Tumblr and Flikr kinda has the same system where they have full-sized photo, a reduced-size photo, and a thumbnail.
 * HTML5 has a new element called <figure> where objects like photos are encapsulated in this special block element along with another element
 * called <figcaption> that displays a caption below the previous element (i.e. <img>)
 * Scaled photos are not as regulated as full-sized photos.
 * If the full-sized photo in photos is deleted, any thumbs or figures should also be deleted.
 * Figure access depends on photo access.
 */
CREATE TABLE IF NOT EXISTS figures (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 photo_id INTEGER(11) NOT NULL,     /* Should be the ID of the photo the thumb represents. */
 name VARCHAR(30) NOT NULL,
 type VARCHAR(30) NOT NULL,
 size INTEGER NOT NULL,             /* in bytes */
 width INTEGER NOT NULL,            /* in pixels */
 height INTEGER NOT NULL,           /* in pixels */
 scale FLOAT NOT NULL,              /* in percent */
 figcap VARCHAR(255),               /* Add a figure caption! */
 content MEDIUMBLOB NOT NULL,
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 PRIMARY KEY(id),
 FOREIGN KEY(photo_id) REFERENCES photos(id),
 FOREIGN KEY(created_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS blogs (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,

 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 access ENUM('private','admins','public') DEFAULT 'private',
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* TODO: Should I create a commenting system for articles? */
/* TODO: How should I publish articles if social media outlets are possible to share stuff? */
CREATE TABLE IF NOT EXISTS articles (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 broadcast_when DATETIME NOT NULL,
 headline VARCHAR(255) NOT NULL,                           /* The title of the article */
 lead VARCHAR(255) NOT NULL,                               /* The description of the article, this will be used in the news letter for a short description */
 contents TEXT NOT NULL,                                   /* The contents of the article. */
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 edited_by INTEGER,
 edited_when TIMESTAMP,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 access ENUM('private','admins','public') DEFAULT 'private',
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id),
 FOREIGN KEY(edited_by) REFERENCES admins(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* This table is for volunteers to submit ideas for events to the adminstrators.
 * This table is for registered volunteers to submit ideas.
 * Anonymous or unregistered users can't do this.
 * I'm still on the fence as to add options to the volunteers table for banning users who pretty much abuse the system.
 * At any rate, this is probably the only thing volunteers can really add to the website, except maybe comments on articles, but there's no system for that set up...yet.
 */
CREATE TABLE IF NOT EXISTS vol_events (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(255) NOT NULL,
 address1 VARCHAR(40),
 address2 VARCHAR(20),
 city VARCHAR(30),
 state VARCHAR(2),
 zip_code VARCHAR(5),
 start_when DATETIME,
 end_when DATETIME,
 description TEXT NOT NULL,
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 approved_by INTEGER,
 approved_when TIMESTAMP,
 pub_status ENUM('draft','review','publish') DEFAULT 'draft',
 pub_news ENUM('no','yes') DEFAULT 'no',
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES volunteers(id),
 FOREIGN KEY(approved_by) REFERENCES admins(id)
);

/* Volunteers control what events they RSVP
 * Assuming that a volunteer's idea for an event is approved.
 */
CREATE TABLE IF NOT EXISTS vol_event_vols (
 evt_id INTEGER,
 vol_id INTEGER,
 vol_status ENUM('no','maybe','yes') DEFAULT 'no',
 vol_rsvp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 FOREIGN KEY(evt_id) REFERENCES vol_events(id),
 FOREIGN KEY(vol_id) REFERENCES volunteers(id),
);

/* Phase 3: Hastagging System */
CREATE TABLE IF NOT EXISTS hashtags (
 id INTEGER(11) NOT NULL AUTO_INCREMENT,
 name VARCHAR(50),
 created_by INTEGER NOT NULL,
 created_when TIMESTAMP NOT NULL,
 PRIMARY KEY(id),
 FOREIGN KEY(created_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS event_tags (
 event_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(event_id) REFERENCES events(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);

/* Even though volunteers can submit ideas for volunteer events, only the admins can hashtag it! */
CREATE TABLE IF NOT EXISTS vol_event_tags (
 event_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(event_id) REFERENCES events(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);


CREATE TABLE IF NOT EXISTS folder_tags (
 folder_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(folder_id) REFERENCES folders(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS file_tags (
 file_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(file_id) REFERENCES files(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS gallery_tags (
 gallery_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(gallery_id) REFERENCES galleries(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS photo_tags(
 photo_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(photo_id) REFERENCES photos(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);

CREATE TABLE IF NOT EXISTS blog_tags (
 blog_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(blog_id) REFERENCES blogs(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)

);

CREATE TABLE IF NOT EXISTS article_tags (
 article_id INTEGER(11) NOT NULL AUTO_INCREMENT,
 tag_id VARCHAR(50),
 tagged_by INTEGER NOT NULL,
 tagged_when TIMESTAMP NOT NULL,
 FOREIGN KEY(article_id) REFERENCES articles(id),
 FOREIGN KEY(tag_id) REFERENCES tags(id),
 FOREIGN KEY(tagged_by) REFERENCES admins(id)
);

/* In order to better understand what users like and read often, it is necessary to log visitor information.
 * See http://www.plus2net.com/php_tutorial/visitor-logging.php to see how we could possibly do that.
 */
CREATE TABLE IF NOT EXISTS track_visitors (

);

/* Phase 4: Make some kind of script (in another file) that scouts for new posts. */